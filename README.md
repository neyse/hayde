# hayde [![PyPi version](https://img.shields.io/pypi/v/hayde.svg)](https://pypi.python.org/pypi/hayde/) [![PyPI pyversions](https://img.shields.io/pypi/pyversions/hayde.svg)](https://pypi.python.org/pypi/hayde/) [![](https://img.shields.io/gitlab/license/f9n/hayde.svg)](https://gitlab.com/f9n/hayde/blob/master/LICENSE)

Hayde

# Installation

```bash
$ python3 -m pip install --user --upgrade hayde
```

# Usage

```bash
$ hayde -h
usage: Hayde [-h] --config-file CONFIG_FILE [--version]

Hayde

optional arguments:
  -h, --help            show this help message and exit
  --config-file CONFIG_FILE
  --version             show program's version number and exit
```
