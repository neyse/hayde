# TODOS

- [X] Poetry for Requirements
- [X] Publish PYPI
- [ ] Add .gitlab-ci.yml for publish the PYPY
- [ ] Create GeneratorOperation class
- [ ] Seperate Operations
- [ ] Watch K8s Resource Changes
- [ ] Tox for Testing
- [ ] Linting, editorconfig
