#!/usr/bin/env bash
# https://gist.github.com/jlazic/e65f5bda141ffaed5640
#This script concatenates multiple files of haproxy configuration into
#one file, and than checks if monolithic config contains errors. If everything is
#OK with new config script will write new config to $CURRENTCFG and reload haproxy


CURRENT_CONFIG=/etc/haproxy/haproxy.cfg
GENERAL_CONFIG=/etc/haproxy/haproxy-general.cfg
CONFIG_DIR=/etc/haproxy/conf.d
NEW_CONFIG=/tmp/haproxy.cfg.tmp


echo "Compiling *.cfg files from $CONFIG_DIR"
ls $CONFIG_DIR/*.cfg
cat $GENERAL_CONFIG $CONFIG_DIR/*.cfg > $NEWCFG
echo "Differences between current and new config"
diff -s -U 3 $CURRENTCFG $NEWCFG | colordiff
if [ $? -ne 0 ]; then
        echo "You should make some changes first :)"
        exit 1 #Exit if old and new configuration are the same
fi
echo -e "Checking if new config is valid..."
haproxy -c -f $NEW_CONFIG

if [ $? -eq 0 ]; then
        echo "Check if there are some warnings in new configuration."
        echo "Working..."
        cat $GENERAL_CONFIG $CONFIG_DIR/*.cfg > $CURRENT_CONFIG
        echo "Reloading haproxy..."
        systemctl reload haproxy
else
        echo "There are errors in new configuration, please fix them and try again."
        exit 1
fi